package exSet1_Logs;

import java.util.Date;
import java.util.Objects;

public class Log {
	private String name;
	private Date dateTime;
	
	public Log(String name, Date date) {
		this.name = name;
		this.dateTime = date;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTima(Date dateTime) {
		this.dateTime = dateTime;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Log other = (Log) obj;
		return Objects.equals(name, other.name);
	}
}
