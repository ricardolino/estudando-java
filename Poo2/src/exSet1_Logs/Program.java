package exSet1_Logs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.time.Instant;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Program {

	public static void main(String[] args) {
		
		String path = "/home/ricardo/Documents/estudando-java/files-exSet/in.txt";
		Set<Log> setLog = new HashSet<>();
		
		try(BufferedReader br = new BufferedReader(new FileReader(path))){
			String line = br.readLine();
			while (line != null) {
				String[] splitLine = line.split(" ");
				setLog.add(new Log(splitLine[0], Date.from(Instant.parse(splitLine[1]))));
				line = br.readLine();
			}
		}catch(IOException e) {
			System.out.println(e.getMessage());
		}finally {
			System.out.println("Total users: " + setLog.size());
		}
	}

}
