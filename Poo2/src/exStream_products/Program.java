package exStream_products;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Program {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		String path = "/home/ricardo/Documents/estudando-java/files-exStream/in.txt";

		try (BufferedReader br = new BufferedReader(new FileReader(path))){
			List<Employee> employees = new ArrayList<>();
			
			String line = br.readLine();
			while (line != null) {
				String[] fields = line.split(",");
				employees.add(new Employee(fields[0], fields[1], Double.parseDouble(fields[2])));
				line = br.readLine();
			}
			
			System.out.print("Enter salary: ");
			double salary = sc.nextDouble();
			
			
			List<String> emails = employees.stream()
					.filter(p -> p.getSalary() > salary)
					.map(p -> p.getEmail())
					.sorted()
					.collect(Collectors.toList());
			
			emails.forEach(System.out::println);
			
			System.out.print("Sum of salary of people whose name starts with 'M': " + employees.stream()
				.filter(p -> p.getName().charAt(0) == 'M')
				.map(p -> p.getSalary())
				.reduce(0.0, (x,y) -> x + y));
		}catch (IOException e) {
			System.out.println(e.getMessage());
		}
		sc.close();
	}

}
