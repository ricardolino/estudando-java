package exMap_election;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class Program {

	public static void main(String[] args) {
		String path = "/home/ricardo/Documents/estudando-java/files-exMap/in.txt";
		
		try (BufferedReader br = new BufferedReader(new FileReader(path))){
			Map<String, Integer> election = new LinkedHashMap<>();
			
			String line = br.readLine();
			while (line != null) {
				String[] splitLine = line.split(",");
				String name = splitLine[0];
				int votes = Integer.parseInt(splitLine[1]);
				
				if (!election.containsKey(name)) {
					election.put(name, votes);
				}else {
					int priorVotes = election.get(name);
					election.put(name, votes + priorVotes);
				}
				
				line = br.readLine();
			}
			
			for (String key : election.keySet()) {
				System.out.println(key + ": " + election.get(key));
			}
		}catch(IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

}
