package InterfacePaymentService;

public class ContractService {
	private OnlinePaymentService onlinePaymentService;
	
	public ContractService(OnlinePaymentService onlinePaymentService) {
		this.onlinePaymentService = onlinePaymentService;
	}

	public void processContract(Contract contract, int months) {
		for(int i = 1; i<months+1; i++) {
			double newAmount = contract.getTotalValue()/months;
			double resultInterest = this.onlinePaymentService.interest(newAmount, i);
			newAmount = newAmount + resultInterest;
			double resultPaymentFee = this.onlinePaymentService.paymentFee(newAmount);
			Installment installment = new Installment(contract.getDate().plusMonths(i), newAmount+resultPaymentFee);
			contract.addInstallment(installment);
		}
	}
}
