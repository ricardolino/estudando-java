package FIles;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Program {

	private static final boolean Produtc = false;

	public static void main(String[] args) {
		List<Product> products = new ArrayList<>();
		
		String path = "/home/ricardo/Documents/estudando-java/files/in.csv";
		
		try(BufferedReader br = new BufferedReader(new FileReader(path))){
			String line = br.readLine();
			while(line != null) {
				products.add(new Product(line.split(",")[0], Double.parseDouble(line.split(",")[1]), Integer.parseInt(line.split(",")[2])));
				line = br.readLine();
			}
		}catch(IOException e) {
			System.out.println(e.getMessage());
		}
		
		
		boolean success = new File("/home/ricardo/Documents/estudando-java/files/" + "out").mkdir();
		
		String pathOut = "/home/ricardo/Documents/estudando-java/files/out/summary.csv";
		try(BufferedWriter bw = new BufferedWriter(new FileWriter(pathOut))){
			for (Product product : products ) {
				String line = product.getName() + "," + product.totalPrice();
				bw.write(line);
				bw.newLine();
			}
		}catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

}
