package com.TaxPayers;

public class Individual extends TaxPayer{

	private Double healthExpenditures;
	
	public Individual(String name, Double anualIncome, Double healthExpenditures) {
		super(name, anualIncome);
		this.healthExpenditures = healthExpenditures;
	}

	public Double getHealthExpenditures() {
		return healthExpenditures;
	}

	public void setHealthExpenditures(Double healthExpenditures) {
		this.healthExpenditures = healthExpenditures;
	}

	@Override
	public double taxPaid() {
		double taxPercent = 25;
		if (this.anualIncome < 20000) {
			taxPercent = 15;
		}
		return (this.anualIncome * (taxPercent/100)) - (this.healthExpenditures * 0.5);
	}

}
