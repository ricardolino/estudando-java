package com.TaxPayers;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		List<TaxPayer>  taxPayers = new ArrayList<>();
		
		System.out.print("Enter the number of tax payers: ");
		int qtdTaxPayers = sc.nextInt();
		
		for (int i=0; i<qtdTaxPayers; i++) {
			System.out.println("Tax payer #" + (i+1) + " data:");
			
			System.out.print("Individual or company (i/c)? ");
			char taxPayer = sc.next().charAt(0);
			
			System.out.print("Name: ");
			String name = sc.next();
			
			System.out.print("Anual income: ");
			double anualIncome = sc.nextDouble();
			
			if (taxPayer == 'i') {
				System.out.print("Health expenditures: ");
				double healthExpenditures = sc.nextDouble();
				taxPayers.add(new Individual(name, anualIncome, healthExpenditures));
			}else if(taxPayer == 'c'){
				System.out.print("Number of employees: ");
				int employeeNumber = sc.nextInt();
				taxPayers.add(new Company(name, anualIncome, employeeNumber));
			}
		}
		
		sc.close();
		
		double totalTaxes = 0.0;
		System.out.println();
		System.out.println("TAXES PAID:");
		for(TaxPayer taxPayer : taxPayers) {
			totalTaxes += taxPayer.taxPaid();
			System.out.printf("%s: $ %.2f%n", taxPayer.getName(), taxPayer.taxPaid());
		}
		
		System.out.println();
		System.out.printf("TOTAL TAXES: $ %.2f%n", totalTaxes);
	}

}
