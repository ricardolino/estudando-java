package com.TaxPayers;

public class Company extends TaxPayer{
	private Integer employeeNumber;

	public Company(String name, Double anualIncome, Integer employeeNumber) {
		super(name, anualIncome);
		this.employeeNumber = employeeNumber;
	}

	public Integer getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(Integer employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	@Override
	public double taxPaid() {
		double taxPercent = 16;
		if (this.employeeNumber > 10) {
			taxPercent = 14;
		}
		return this.anualIncome * (taxPercent / 100);
	}
	
}
