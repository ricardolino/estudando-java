package com.paymentEmployees;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Program {
	public static void main (String[] args){
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		List<Employee> employeeList = new ArrayList<>();
		
		System.out.print("Enter the number of employees: ");
		int numberEmployess = sc.nextInt();
		
		for (int i=0; i<numberEmployess; i++) {
			System.out.println("Employee #" + (i+1) + " data:");
			System.out.print("Outsourced (y/n)? ");
			char outsourced = sc.next().charAt(0);
			System.out.print("Name: ");
			String name = sc.next();
			System.out.print("Hours: ");
			int hours = sc.nextInt();
			System.out.print("Values per hour: ");
			double valuePerHour = sc.nextDouble();
			
			if (outsourced == 'y') {
				System.out.print("Additional charge: ");
				Double additionalCharge = sc.nextDouble();
				employeeList.add(new OutsourceEmployee(name,hours,valuePerHour, additionalCharge));
			}else {
				employeeList.add(new Employee(name,hours,valuePerHour));
			}
		}
		
		sc.close();
		
		System.out.println();
		System.out.println("PAYMENT:");
		for (Employee employee : employeeList) {
			System.out.println(employee.getName() + " - " + employee.payment());
		}
	}
}
