package com.Persionato;

import java.util.Scanner;

public class Persionato {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("How many rooms will be rented? ");
		int qtd_rents = sc.nextInt();
		sc.nextLine();
		entidade[] dadosPessoas = new entidade[10];
		
		for(int i=0; i<qtd_rents; i++) {
			System.out.println("Rent #" + (i+1) + ":");
			System.out.print("Name: ");
			String name = sc.nextLine();
			System.out.print("Email: ");
			String email = sc.nextLine();
			System.out.print("Room: ");
			int room = sc.nextInt();
			sc.nextLine();
			
			entidade dadosPessoa = new entidade(name, email);
			dadosPessoas[room] = dadosPessoa;
		}
		
		System.out.println();
		System.out.println("Busy rooms: ");
		for(int i=0; i<10; i++) {
			if(dadosPessoas[i]!=null) {
				System.out.println(i + ": " + dadosPessoas[i].getName() + ", " + dadosPessoas[i].getEmail());
			}
		}
		
		sc.close();
	}
}
