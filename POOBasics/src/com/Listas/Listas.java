package com.Listas;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Listas {
	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		
		list.add("Maria");
		list.add("Alex");
		list.add("Bob");
		list.add("Anna");
		list.add(2, "Marco");
		/*
		 * .add(value) ou .add(index, value)
		 * 	- Permite adicionar valores a lista
		 * 	- Para adcionar é necessário somente informar o valor ou informar o valor e index específico que deseja adicionar o valor
		 */
		
		System.out.println(list.get(0)); //Pegar valor específico
		
		System.out.println(list.size()); //.size - Retorna o tamanho da lista
				
		for (String x : list) {
			System.out.println(x);
		}
		
		System.out.println("---------------------");
		list.removeIf(x -> x.charAt(0) == 'M');
		for (String x : list) {
			System.out.println(x);
		}
		/*
		 * .remove(index or value)
		 * 	- Remove elementos da lista
		 * 	- Para remover é necessário infomrar o index ou o próprio valor que deseja remover
		 * 
		 * .removeIf(x -> condition)
		 * 	- Remove elementos da lista de acordo com a condição (lambda) criada
		 */
		
		System.out.println("---------------------");
		System.out.println("Index of Bob: " + list.indexOf("Bob"));
		System.out.println("Index of Marco: " + list.indexOf("Marco"));
		/*
		 * .indexOf(value) - Retorna o endereço do valor
		 * 	- Caso o valor não exista dentro da lista irá retorna -1*/
		
		System.out.println("---------------------");
		List<String> result = list.stream().filter(x -> x.charAt(0) == 'A').collect(Collectors.toList());
		for (String x : result) {
			System.out.println(x);
		}
		/*
		 * .stream() - Converte a lista para stream, para utlizar a função lambda e criar filtros
		 * .filter(x -> condition) - Criar um filtro dos elementos que forem verdadeiros de acordo com condição
		 * .collect(Collectors.toList()) - Converte para list
		 * Toda essa operação faz o contrário do removeIf, ou seja, mantém os elementos que passarem pelo filtro
		 * */
		
		System.out.println("---------------------");
		String name = list.stream().filter(x -> x.charAt(0) == 'J').findFirst().orElse(null);
		System.out.println(name);
		/*
		 * .findFirst() - Encontra o primeiro elemento de acorodo com o filtro criado
		 * .orElse(null) - Retorno null caso não encontre valor procurado
		*/
	}
}
