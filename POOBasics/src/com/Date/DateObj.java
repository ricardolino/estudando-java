package com.Date;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateObj {

	public static void main(String[] args) {

		// https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/time/format/DateTimeFormatter.html
		DateTimeFormatter fmt1 = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		DateTimeFormatter fmt2 = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

		LocalDate LocalDateNow = LocalDate.now();
		LocalDateTime LocalDateTimeNow = LocalDateTime.now();
		Instant instantNow = Instant.now();

		LocalDate localDateParse = LocalDate.parse("2022-07-20");
		LocalDateTime localDateTimeParse = LocalDateTime.parse("2022-07-20T01:30:26");
		Instant instantParse = Instant.parse("2022-07-20T01:30:26Z");
//		Instant d07 = Instant.parse("2022-07-20T01:30:26-03:00");
//		
//		// https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/time/format/DateTimeFormatter.html
		LocalDate localDateParseFormatter = LocalDate.parse("20/07/2022", fmt1);
		LocalDateTime localDateTimeParseFormatter = LocalDateTime.parse("20/07/2022 01:30", fmt2);
//
		LocalDate LocalDateOf = LocalDate.of(2022, 07, 20);
		LocalDateTime LocalDateTimeOf = LocalDateTime.of(2022, 07, 20, 1, 30);

		System.out.println("LocalDateNow = " + LocalDateNow.toString());
		System.out.println("LocalDateTimeNow = " + LocalDateTimeNow.toString());
		System.out.println("instantNow = " + instantNow.toString());
		System.out.println("localDateParse = " + localDateParse.toString());
		System.out.println("localDateTimeParse = " + localDateTimeParse.toString());
		System.out.println("instantParse = " + instantParse.toString());
//		System.out.println("d07 = " + d07.toString());
		System.out.println("localDateParseFormatter = " + localDateParseFormatter.toString());
		System.out.println("localDateTimeParseFormatter = " + localDateTimeParseFormatter.toString());
		System.out.println("LocalDateOf = " + LocalDateOf.toString());
		System.out.println("LocalDateTimeOf = " + LocalDateTimeOf.toString());

/////////////////////////////////////////////////////////////////////////////////////////
		System.out.println();
		LocalDate d04 = LocalDate.parse("2022-07-20");
		LocalDateTime d05 = LocalDateTime.parse("2022-07-20T01:30:26");
		Instant d06 = Instant.parse("2022-07-20T01:30:26Z");

// https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/time/format/DateTimeFormatter.html
		DateTimeFormatter DateTimeFormatter1 = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		DateTimeFormatter DateTimeFormatter2 = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
		DateTimeFormatter DateTimeFormatter3 = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")
				.withZone(ZoneId.systemDefault());
		DateTimeFormatter DateTimeFormatter4 = DateTimeFormatter.ISO_DATE_TIME;
		DateTimeFormatter DateTimeFormatter5 = DateTimeFormatter.ISO_INSTANT;

		System.out.println("d04 to DateTimeFormatter1 = " + d04.format(DateTimeFormatter1));
		System.out.println("DateTimeFormatter to d04 = " + DateTimeFormatter1.format(d04));
		System.out.println("d04 to DateTimeFormatter= " + d04.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));

		System.out.println("d05 to DateTimeFormatter1 = " + d05.format(DateTimeFormatter1));
		System.out.println("d05 to DateTimeFormatter2 = " + d05.format(DateTimeFormatter2));
		System.out.println("d05 to DateTimeFormatter4 = " + d05.format(DateTimeFormatter4));

		System.out.println("DateTimeFormatter3 to d06 = " + DateTimeFormatter3.format(d06));
		System.out.println("DateTimeFormatter5 to d06 = " + DateTimeFormatter5.format(d06));
		System.out.println("d06 = " + d06.toString());

//////////////////////////////////////////////////////////////////////////////////////////

		LocalDate d07 = LocalDate.parse("2022-07-20");
		LocalDateTime d08 = LocalDateTime.parse("2022-07-20T01:30:26");
		Instant d09 = Instant.parse("2022-07-20T01:30:26Z");

		LocalDate r1 = LocalDate.ofInstant(d09, ZoneId.systemDefault());
		LocalDate r2 = LocalDate.ofInstant(d09, ZoneId.of("Portugal"));
		LocalDateTime r3 = LocalDateTime.ofInstant(d09, ZoneId.systemDefault());
		LocalDateTime r4 = LocalDateTime.ofInstant(d09, ZoneId.of("Portugal"));

		System.out.println("r1 = " + r1);
		System.out.println("r2 = " + r2);
		System.out.println("r3 = " + r3);
		System.out.println("r4 = " + r4);

		System.out.println("d07 dia = " + d07.getDayOfMonth());
		System.out.println("d07 mês = " + d07.getMonthValue());
		System.out.println("d07 ano = " + d07.getYear());

		System.out.println("d08 hora = " + d08.getHour());
		System.out.println("d08 minutos = " + d08.getMinute());
	}
}
