package com.BoxingUnboxingWrapper;

public class BoxingUnboxingWrapper {
	public static void main(String[] args) {
		int x = 20;
		
		//Object obj = x; //Boxing
		Integer obj = x; //Boxing - Utilizando Wrapper Classes
		
		//int y = (int) obj; //Unboxing - é necessário o casting devido ao fato de Object não ser diretamente um inteiro
		int y = obj; //Não tem necessidade do casting por ter sido declado usando Wrapper classes 
		
		System.out.println(y);
	}
}

/*
 * Boxing 
 * 	- É o processo de conversão de um objeto tipo valor para um objeto
tipo referência compatível
 * Unboxing 
 * 	- É o processo de conversão de um objeto tipo referência para um
objeto tipo valor compatível
 * Wrapper classes: 
 * 	- São classes equivalentes aos tipos primitivos
 * 	- Uso comum: campos de entidades em sistemas de informação
 * 		- Pois tipos referência (classes) aceitam valor null e usufruem dos recursos OO
 * */
