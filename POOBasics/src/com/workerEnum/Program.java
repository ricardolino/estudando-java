package com.workerEnum;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Program {

	public static void main(String[] args) {
		Worker testWorker = new Worker("Alex", WorkerLevel.valueOf("MID_LEVEL"), 1200.00, new Department("Design"));
		
		System.out.println(testWorker.getName() +" "+ 
						   testWorker.getLevel()  +" "+ 
						   testWorker.getBaseSalary() +" "+
						   testWorker.getDepartment().getName()
							);
		
		DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		testWorker.addContract(new HourContract(LocalDate.parse("20/08/2018", fmt), 50.00, 20));
		testWorker.addContract(new HourContract(LocalDate.parse("13/06/2018", fmt), 30.00, 18));
		testWorker.addContract(new HourContract(LocalDate.parse("25/08/2018", fmt), 80.00, 10));
		
		System.out.println(testWorker.income(8, 2018));
	}

}
