package com.workerEnum;

import java.util.ArrayList;
import java.util.List;

public class Worker {
	private String name;
	private WorkerLevel level;
	private Double baseSalary;
	
	private Department department;
	
	private List<HourContract> hourContracts = new ArrayList<>();

	public Worker(String name, WorkerLevel level, Double baseSalary, Department department) {
		this.name = name;
		this.level = level;
		this.baseSalary = baseSalary;
		this.department = department;
	}
	
	
	
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public WorkerLevel getLevel() {
		return level;
	}



	public void setLevel(WorkerLevel level) {
		this.level = level;
	}



	public Double getBaseSalary() {
		return baseSalary;
	}



	public void setBaseeSalary(Double baseeSalary) {
		this.baseSalary = baseeSalary;
	}



	public Department getDepartment() {
		return department;
	}



	public void setDepartment(Department department) {
		this.department = department;
	}



	public List<HourContract> getHourContracts() {
		return hourContracts;
	}

	public void addContract(HourContract contract) {
		this.hourContracts.add(contract);
	}
	
	public void removeContract(HourContract contract) {
		this.hourContracts.remove(contract);
	}
	
	public Double income(int month, int year) {
		double sum = this.baseSalary;
//		System.out.println(
//				hourContracts.get(0).getDate() +" "+
//				hourContracts.get(0).totalValue()
//				);
//		System.out.println(
//				hourContracts.get(1).getDate() +" "+
//				hourContracts.get(1).totalValue()
//				);
		for(HourContract c: this.hourContracts) {
//			System.out.println(c.getDate().getMonthValue() +" = "+ month);
//			System.out.println(c.getDate().getYear() +" = "+ year);
			if (c.getDate().getMonthValue() == month && c.getDate().getYear() == year) {
				sum += c.totalValue();
			}
		}
		
		return sum;
	}
}
