package com.hello;

import java.util.Scanner;

public class InData {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		String varString;
		int varInt;
		double varDouble;
		char varChar;
		
		varString = sc.next();
		varInt = sc.nextInt();
		varDouble = sc.nextDouble();
		varChar = sc.next().charAt(0);
		
		System.out.println("DADOS DIGITADOS:");
		System.out.println(varString);
		System.out.println(varInt);
		System.out.println(varDouble);
		System.out.println(varChar);
		sc.close();
	}
}
