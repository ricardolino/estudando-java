package com.hello;

public class Casting {

	public static void main(String[] args) {
		double bDouble, BDouble, hDouble, areaDouble;
		bDouble = 6.0;
		BDouble = 8.0;         //No caso de double é sempre necessário informar no mínimo o .0
		hDouble = 5.0;
		areaDouble = (bDouble + BDouble) / 2.0 * hDouble;
		System.out.println(areaDouble);
		
		float bFloat, BFloat, hFloat, areaFloat;
		bFloat = 6f;
		BFloat = 8f;          //No caso de float é sempre necessário informar no mínimo o f
		hFloat = 5f;
		areaFloat = (bFloat + BFloat) / 2f * hFloat;
		System.out.println(areaFloat);
		
		int a, b;
		double resultado;
		a = 5;
		b = 2;
		resultado = (double) a / b;
		System.out.println(resultado);

	}

}
