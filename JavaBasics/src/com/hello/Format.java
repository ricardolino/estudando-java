package com.hello;
import java.util.Locale;

public class Format {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US); //Estabelecer formatação de digito decimais
		
		System.out.println("Imprimir em linhas diferentes");
		System.out.print("Imprimir na mesma linha, ");
		System.out.println("Imprimir na mesma linha");
		double x = 10.13548;
		System.out.printf("%.2f%n", x);
	}

}

